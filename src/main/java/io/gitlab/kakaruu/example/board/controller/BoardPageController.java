package io.gitlab.kakaruu.example.board.controller;

import io.gitlab.kakaruu.example.board.service.BoardService;
import io.gitlab.kakaruu.example.board.vo.BoardVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/board")
public class BoardPageController {
  private static final Logger logger = LoggerFactory.getLogger(BoardPageController.class);

  private static final int COUNT_OF_POSTS_PER_PAGE = 10;
  private static final int PAGEBAR_SIZE = 10;

  @Autowired
  BoardService boardService;

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String getBoardList(
      @RequestParam(value = "page", required = false) Integer page,
      Model model
  ) {
    if (page == null ||
        page < 1
    ) {
      page = 1;
    }
    try {
      long countOfPosts = boardService.getCount();
      int lastPageNum = (int) (countOfPosts / COUNT_OF_POSTS_PER_PAGE) + ((countOfPosts % COUNT_OF_POSTS_PER_PAGE) == 0 ? 0 : 1);
      if(lastPageNum == 0) {
        lastPageNum = 1;
      }
      int pagebarStarNum = page - ((page - 1) % PAGEBAR_SIZE);
      int pagebarEndNum = Math.min(pagebarStarNum + PAGEBAR_SIZE - 1, lastPageNum);

      long offset = (page - 1) * COUNT_OF_POSTS_PER_PAGE;
      model.addAttribute("boardList", boardService.getPostList(offset, COUNT_OF_POSTS_PER_PAGE));
      model.addAttribute("page", page);
      model.addAttribute("pagebarStarNum", pagebarStarNum);
      model.addAttribute("pagebarEndNum", pagebarEndNum);
      model.addAttribute("lastPageNum", lastPageNum);
      model.addAttribute("countOfPosts", countOfPosts);
      model.addAttribute("pagebarSize", PAGEBAR_SIZE);
    } catch (Exception e) {
      logger.error("GET /board/list[?page=long] 에러", e);
    }
    return "board/list";
  }

  @RequestMapping(value = "/new", method = RequestMethod.GET)
  public String newBoard(
      @RequestParam(value = "pid", required = false) Long parentId,
      Model model
  ) {
    model.addAttribute("parentId", parentId);
    return "board/newForm";
  }

  @RequestMapping(value = "/new", method = RequestMethod.POST)
  public String postNewBoard(
      @RequestParam(value = "pid", required = false) Long parentId,
      @RequestParam(value = "writer") String writer,
      @RequestParam(value = "title") String title,
      @RequestParam(value = "content") String content,
      Model model
  ) throws Exception {
    BoardVO result = boardService.createNewPost(parentId, title, content, writer);
    if (result != null) {
      return "redirect:/board/view?id=" + result.getId();
    }
    throw new Exception("글 추가 실패");
  }

  @RequestMapping(value = "/view", method = RequestMethod.GET)
  public String getBoard(
      @RequestParam("id") Long id,
      Model model
  ) {
    BoardVO result = boardService.getPost(id);
    model.addAttribute("post", result);
    return "board/view";
  }

  @RequestMapping(value = "/remove", method = RequestMethod.GET)
  public String removeBoard(
      @RequestParam("id") Long id
  ) throws Exception {
    BoardService.DeleteResult result = boardService.deletePost(id);
    if(result == BoardService.DeleteResult.DELETED || result == BoardService.DeleteResult.FLAGGED) {
      return "redirect:/board/list";
    }
    throw new Exception("글 삭제 실패");
  }

  @RequestMapping(value = "/modify", method = RequestMethod.GET)
  public String modifyBoard(
      @RequestParam(value = "id", required = false) Long id,
      Model model
  ) {
    BoardVO result = boardService.getPost(id);
    model.addAttribute("post", result);
    return "board/modForm";
  }

  @RequestMapping(value = "/modify", method = RequestMethod.POST)
  public String postModifyBoard(
      @RequestParam(value = "id") Long id,
      @RequestParam(value = "title") String title,
      @RequestParam(value = "content") String content
  ) throws Exception {
    if (boardService.modifyPost(id, title, content)) {
      return "redirect:/board/view?id=" + id;
    }
    throw new Exception("글 수정 실패");
  }
}
