package io.gitlab.kakaruu.example.board.controller.api;

import io.gitlab.kakaruu.example.board.service.BoardService;
import io.gitlab.kakaruu.example.board.vo.BoardVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BoardController {
  private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

  private static final int PAGE_PER_COUNT = 10;

  @Autowired
  BoardService boardService;

  @RequestMapping(value = "/board", method = RequestMethod.POST)
  public ResponseEntity postBoard(
      @RequestParam(value = "parentId", required = false) Long parentId,
      @RequestParam("title") String title,
      @RequestParam("content") String content,
      @RequestParam("writer") String writer
  ) {
    try {
      BoardVO result = boardService.createNewPost(parentId, title, content, writer);
      return new ResponseEntity(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/board 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/board/{id}", method = RequestMethod.PUT)
  public ResponseEntity putBoard(
      @PathVariable("id") Long id,
      @RequestParam("title") String title,
      @RequestParam("content") String content
  ) {
    try {
      if(boardService.modifyPost(id, title, content)) {
        return new ResponseEntity(id, HttpStatus.OK);
      }
      return new ResponseEntity(id, HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
      logger.error("PUT /api/board 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/board/{id}", method = RequestMethod.DELETE)
  public ResponseEntity deleteBoard(
      @PathVariable("id") Long id
  ) {
    try {
      BoardService.DeleteResult result = boardService.deletePost(id);
      return new ResponseEntity(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/board 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/board/size", method = RequestMethod.GET)
  public ResponseEntity getBoardSize() {
    try {
      long result = boardService.getCount();
      return new ResponseEntity(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/board/size 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/board", method = RequestMethod.GET)
  public ResponseEntity getBoardList(
      @RequestParam(value = "page", required = false) Long page
  ) {
    if(page == null ||
        page < 1
    ) {
      page = 1L;
    }
    long offset = (page - 1) * PAGE_PER_COUNT;

    try {
      List<BoardVO> result = boardService.getPostList(offset, PAGE_PER_COUNT);
      return new ResponseEntity(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/board[?page=long] 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/board/{id}", method = RequestMethod.GET)
  public ResponseEntity getBoard(
      @PathVariable("id") Long id
  ) {
    try {
      BoardVO result = boardService.getPost(id);
      return new ResponseEntity(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/board/" + id + " 에러", e);
      return new ResponseEntity("서버 내부 에러: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
