package io.gitlab.kakaruu.example.board.repository;

import io.gitlab.kakaruu.example.board.vo.BoardVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BoardDAO {
  public int insert(BoardVO newBoard);

  public long getLastInsertId();

  public int updatePrepare(long id);

  public long calcNewChildOrder(BoardVO parent);

  public int plusOrderNext(long groupId, long orderInGroup);

  public int subOrderNext(long groupId, long orderInGroup);

  public int deleteLeaf(long id);

  public int setDeleted(long id);

  public int updateContents(BoardVO modBoard);

  public long selectCount();

  public List<BoardVO> selectInRange(long offset, int count);

  public BoardVO selectOne(long id);

  public Long findParentId(long id);
}
