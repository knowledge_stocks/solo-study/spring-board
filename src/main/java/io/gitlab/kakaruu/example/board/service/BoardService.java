package io.gitlab.kakaruu.example.board.service;

import io.gitlab.kakaruu.example.board.vo.BoardVO;

import java.util.List;

public interface BoardService {
  public BoardVO createNewPost(Long parentId, String title, String content, String writer) throws Exception;

  public boolean modifyPost(long id, String title, String content);

  public DeleteResult deletePost(long id) throws Exception;

  public Long getParentId(long childId);

  public long getCount();

  public List<BoardVO> getPostList(long offset, int count);

  public BoardVO getPost(long id);

  public enum DeleteResult {
    DELETED,
    FLAGGED
  }
}
