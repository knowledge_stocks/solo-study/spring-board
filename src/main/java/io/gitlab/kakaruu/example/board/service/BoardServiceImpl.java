package io.gitlab.kakaruu.example.board.service;

import io.gitlab.kakaruu.example.board.repository.BoardDAO;
import io.gitlab.kakaruu.example.board.vo.BoardVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BoardServiceImpl implements BoardService {
  @Autowired
  BoardDAO boardDAO;

  @Override
  public BoardVO createNewPost(Long parentId, String title, String content, String writer) throws Exception {
    BoardVO newPost = new BoardVO();
    newPost.setTitle(title);
    newPost.setContent(content);
    newPost.setWriter(writer);

    if(parentId != null) {
      BoardVO parent = boardDAO.selectOne(parentId);
      newPost.setOrderInGroup(boardDAO.calcNewChildOrder(parent));
      newPost.setGroupId(parent.getGroupId());
      newPost.setDepth(parent.getDepth() + 1);

      boardDAO.plusOrderNext(newPost.getGroupId(),newPost.getOrderInGroup());
    } else {
      newPost.setOrderInGroup(0L);
      newPost.setDepth(0);
    }

    if(boardDAO.insert(newPost) > 0) {
      long id = boardDAO.getLastInsertId();
      newPost.setId(id);
      if(parentId == null) {
        if(boardDAO.updatePrepare(id) > 0) {
          newPost.setGroupId(id);
        } else {
          throw new Exception("updatePrepare failed id: " + id);
        }
      }
      return newPost;
    }
    throw new Exception("insert failed");
  }

  @Override
  public boolean modifyPost(long id, String title, String content) {
    BoardVO modBoard = new BoardVO();
    modBoard.setId(id);
    modBoard.setTitle(title);
    modBoard.setContent(content);
    return boardDAO.updateContents(modBoard) > 0;
  }

  @Override
  public DeleteResult deletePost(long id) throws Exception {
    BoardVO target = boardDAO.selectOne(id);
    return deleteRecur(target);
  }

  private DeleteResult deleteRecur(BoardVO target) throws Exception {
    Long parentId = null;
    if(target.getDepth() > 0) {
      parentId = getParentId(target.getId());
    }
    if(boardDAO.deleteLeaf(target.getId()) > 0) {
      boardDAO.subOrderNext(target.getGroupId(), target.getOrderInGroup());
      if(parentId != null) {
        BoardVO parent = boardDAO.selectOne(parentId);
        if(parent.getIsDeleted() != null && parent.getIsDeleted()) {
          return deleteRecur(parent);
        }
      }
      return DeleteResult.DELETED;
    } else if(boardDAO.setDeleted(target.getId()) > 0) {
      return DeleteResult.FLAGGED;
    }
    throw new Exception("삭제 실패 id:" + target.getId());
  }

  @Override
  public Long getParentId(long childId) {
    return boardDAO.findParentId(childId);
  }

  @Override
  public long getCount() {
    return boardDAO.selectCount();
  }

  @Override
  public List<BoardVO> getPostList(long offset, int count) {
    return boardDAO.selectInRange(offset, count);
  }

  @Override
  public BoardVO getPost(long id) {
    return boardDAO.selectOne(id);
  }
}
