package io.gitlab.kakaruu.example.board.vo;

import lombok.Data;

import java.util.Date;

@Data
public class BoardVO {
  private Long id;
  private Long groupId;
  private Long orderInGroup;
  private Integer depth;
  private String title;
  private String content;
  private String writer;
  private Date regDateTime;
  private Date lastModDateTime;
  private Boolean isDeleted;
  private Boolean isBlocked;
}
